package Main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class UI extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	final JFileChooser fileChooser;
	final JButton play,pause,stop,open,delete;
	
	public UI() {
		play = new JButton("Play");
		pause = new JButton("Pause");
		stop = new JButton("Stop");
		open = new JButton("Open");
		delete = new JButton("Delete");
		JPanel topPanel = new JPanel(new GridLayout(1, 5));
		topPanel.add(open);topPanel.add(delete);
		topPanel.add(play);topPanel.add(pause);topPanel.add(stop);
		
		
		JPanel center = new JPanel(){
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(200, 200);
            }
        };
        center.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.add(topPanel, BorderLayout.NORTH);
        this.add(center, BorderLayout.SOUTH);
        open.addActionListener(this);
		fileChooser = new JFileChooser();
		setTitle("Simple example");
		setSize(300, 200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

    public static void main(String[] args) {
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                UI ex = new UI();
                ex.setVisible(true);
            }
        });
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    	
    	if(e.getSource() == open){ 
	    	int returnVal = fileChooser.showOpenDialog(UI.this);
	
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File file = fileChooser.getSelectedFile();
	            //This is where a real application would open the file.
	            System.out.println("Opening: " + file.getName());
	        } else {
	        	System.out.println("Open command cancelled by user.");
	        }
    	}
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //ICON STATIC LOADS:
    
    
    
    
    
    
    
    
    
}
