package Logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import NetBase.ManagedConnection;
import NetBase.ManagedConnection.ConnectionState;
import NetBase.MessageParser.Request;
import NetBase.MessageParser.Response;
import Primitives.BitMap.Rarity;
import Primitives.Piece;
import TorrentData.Torrent;

/**
 * This is the implementation of peer logic
 * This is probably the most basic form of a client.
 * 
 * It follows the following rules:
 * -if some one requests something that we can give:
 * 	*give it to them
 * -collect list of rarity pieces we dont have and disseminate get requests for them 
 * 
 * 
 * 
 * @author wiselion
 *
 */
public class BasicPeerLogic extends PeerLogic{
	int recvPieces = 0;
	int activeConnections =0;
	private final static int MAX_QUEUE_SIZE = 20;//TODO: 5*active connections
	private static int MAX_ENQEUED = 5;
	private static int MAX_ACTIVE_PIECES = 2;//TODO: be grown like tcp
	private static int MAX_ACTIVE_REQUESTS = 10;
	
	private boolean exhausted = false;
	private long rarityTimer = System.currentTimeMillis();
	private long haveAccumulator = 0;
	private final long MIN_RARITY_TIME = 1000;
	private Disseminator disseminator = new Disseminator();
	
	private void connectionCleanUp(ManagedConnection mc){
		disseminator.connectionCleanUp(mc);
		try {mc.shutDown();} catch (IOException e) {}
	}

	@Override
	public void doWork(Torrent t) throws IOException {
		activeConnections =0;
		Set<ManagedConnection> pList = t.getPeers();
		Iterator<ManagedConnection> itor = pList.iterator();
		while(itor.hasNext()){
			ManagedConnection mc = itor.next();
			if(mc.getConnectionState() == ConnectionState.uninitialized){
				mc.initalizeConnection(t.pm.bitmap.getMapCopy(),t);
				disseminator.initializeConnection(mc);
				t.pm.bitmap.addPeerMap(mc.getPeerBitmap());//adds
			}else if(mc.getConnectionState() == ConnectionState.closed){
				//pull off anything new?
				itor.remove();
				connectionCleanUp(mc);
				haveAccumulator=1;//just set so can recalculate.
				t.pm.bitmap.removePeerMap(mc.getPeerBitmap());
			}else{
				mc.doWork(t);
			}
			
			if(mc.getConnectionState() == ConnectionState.connected){
				//Read state. 
				//Push requests (if allowed)
				//Push blocks (if allowed)
				activeConnections++;
				haveAccumulator+=mc.haveSinceLastCall();
				if (mc.peerInterested()) {
//					for (Piece piece : completedLast) {
//						mc.pushHave((int) piece.pieceIndex);
//					}
				}
			}

			
				
			
		}
		
		//TODO: something about not write values being thrown in loop
		//TODO: check for recently called. We may just have everything that we can get.
		//TODO: timer, delta have's
		//Recomputes rarity based on time and new have's
		if((System.currentTimeMillis()-rarityTimer)>MIN_RARITY_TIME && (haveAccumulator>0)){
			t.pm.bitmap.recomputeRarity();//TODO: Dont recompute so often too cpu intensive.
			haveAccumulator = 0;
			rarityTimer = System.currentTimeMillis();
			exhausted = false;
		}
		
		
		//Sets Completion queue by rarity.
		//TODO: client may leave, access to piece might disapear..
		if(workingQueue.size() == 0 && !t.pm.bitmap.isComplete()&&!exhausted){//&&!exhausted
			workingQueue.clear();
			List<Rarity> rList = t.pm.bitmap.getRarity();
			boolean addedOnce = false;
			for(Rarity rar: rList){
				if(workingQueue.size()>=MAX_QUEUE_SIZE){
					addedOnce=true;
					break;
				}
				//if some one has it and we don't and were not working to get it yet.
				if(rar.getCount()>0 && !t.pm.hasPiece(rar.index) && !disseminatedPiecesToCompete.containsKey(rar.index)){
					workingQueue.add(t.pm.bitmap.createPiece(rar.index));
					addedOnce = true;
				}else if(rar.getCount()==0){
					System.out.println("No one has "+rar.index);
				}
			}
			exhausted=!addedOnce;
		}
		
		//TODO: do something about multiple clients working on same part.
		//Dissemination from primary queue.
		Iterator<Piece> pQueue = workingQueue.iterator(); 
		while(pQueue.hasNext()){
			Piece p = pQueue.next();
			boolean taken = false;
			if(disseminatedPiecesToCompete.containsKey((int)p.pieceIndex)){
				pQueue.remove();
				continue;
			}
			for(ManagedConnection mc : pList){
				if(mc.getConnectionState() == ConnectionState.connected){
					ConnectionWork cw = clientToPieceSet.get(mc);
					
					if(cw.piecesToComplete.size()<=MAX_ENQEUED && mc.getPeerBitmap().hasPiece((int)p.pieceIndex)){
						taken=true;
						pQueue.remove();
						disseminatedPiecesToCompete.put((int)p.pieceIndex, p);
						if(!cw.piecesToComplete.containsKey(p.pieceIndex)){
							System.out.println("Qeued up: "+p.pieceIndex);
							cw.piecesToComplete.put((int)p.pieceIndex,p);
						}
						break;
					}
				}
			}
			if(!taken){
				break;//everyone's queue'd up.
			}
		}
		
		if(System.currentTimeMillis()%1000==0){
			System.out.println("Active Connections: "+activeConnections);
		}
		
		t.pm.doBlockingWork();//TODO: remove from here. set to threaded process.
	}
	

}
