package Primitives;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * Primitive data structure used to determine:
 * 1. Optimal pieces to get (rarity)
 * 2. A sharable data structure that is immutable.
 * MAX PEERS: 32k
 * @note So i edited this to make it compatible with partial updates. 
 */
public class BitMap {
	byte [] ourBitMap;
	public class Rarity implements Comparable<Rarity>{
		private short value;
		public final int index;
		public Rarity(int index){this.index=index;}
		@Override
		public int compareTo(Rarity o) {
			return o.value-value;
		}
		public short getCount(){return value;}
	}
	Map<Integer,Rarity> indexToRarity;
	List<Rarity> rarity;
	short [] rarityMap;
	int pieceLength;
	long totalData;
	int numPieces;
	List<BitMap> peerMaps;

	public BitMap(long totalData, int pieceLength){
		numPieces = (int) Math.ceil(totalData/pieceLength);
		ourBitMap = new byte[(int)Math.ceil(numPieces/8.0)];
		peerMaps = new ArrayList<BitMap>();
		rarity = new ArrayList<Rarity>();
		indexToRarity= new HashMap<Integer,Rarity>();
		rarityMap = new short[numPieces];
		for(int i=0;i<numPieces;i++){
			Rarity r = new Rarity(i);
			indexToRarity.put(r.index,r);
			rarity.add(r);
		}
		this.pieceLength = pieceLength;
		
	}
	
	public int getNumberOfPieces(){
		return numPieces;
	}
	
	public long getTotalSize(){
		return totalData;
	}
	
	public void addPieceComplete(long i){
		int index = (int) i;
		byte  b= ourBitMap[index/8];
		b = (byte) (b | (1 << index%8));
		ourBitMap[index/8] = b;
	}
	
	public boolean hasPiece(int index){
		boolean b = (ourBitMap[index/8] & (1 << index%8)) != 0;
		return b;
	}
	
	/***
	 * O(peers*pieces)
	 * Grows linearly with number of peers
	 * 
	 * TODO: recompute on new Have messages.
	 * This absolutely bonkers.
	 * @deprecated
	 */
	public void recomputeRarity(){
		for(int i=0;i<rarityMap.length;i++){
			rarityMap[i]=0;
		}
		for(int i=0;i<numPieces;i++){
			for(BitMap b: peerMaps){
				rarityMap[i] +=b.hasPiece(i)?1:0;
			}
			indexToRarity.get(i).value=rarityMap[i];
		}
		Collections.sort(rarity);
	}
	
	public void setBitMap(byte [] bitmap){
		if(bitmap.length != this.ourBitMap.length){
			throw new RuntimeException("invalid bitmap length");
		}
		for(int i=0;i<bitmap.length;i++){
			this.ourBitMap[i]=bitmap[i];
		}
	}
	
	public int getLength(){
		return ourBitMap.length;
	}
	
	public byte[] getMapCopy(){
		return ourBitMap.clone();
	}
	
	public List<Rarity> getRarity(){
		return rarity;
	}
	
	public Piece createMissingPiece(int index){
		int pSize = pieceLength;
		if(index == numPieces-1){
			pSize = (int) (totalData%pieceLength);//final piece!
		}
		Piece p =new Piece(index,pSize);
		return p;
	}
	
	
	/**
	 * Recomputes rarity.
	 * "If the input array is nearly sorted, the implementation requires approximately n comparisons"
	 * @param bm
	 * @return
	 */
	public void haveUpdate(int i,BitMap b){
		rarityMap[i]+=1;
		indexToRarity.get(i).value=rarityMap[i];
		Collections.sort(rarity);
	}
	
	private void reCompute(BitMap b,boolean a){
		for(int i=0;i<ourBitMap.length;i++){
			if(b.hasPiece(i)){
				rarityMap[i]+=a?1:-1;
				indexToRarity.get(i).value=rarityMap[i];
			}
		}
		Collections.sort(rarity);
	}
	
	/**
	 * Recomputes rarity.
	 * @param bm
	 * @return
	 */
	public void addPeerMap(BitMap bm){
		if(!peerMaps.contains(bm)){
			reCompute(bm,false);
			peerMaps.add(bm);
		}
	}
	
	/**
	 * Recomputes rarity.
	 * @param bm
	 * @return
	 */
	public boolean removePeerMap(BitMap bm){
		if(peerMaps.contains(bm)){
			reCompute(bm,false);
		}
		return peerMaps.remove(bm);
	}
	
	
}
