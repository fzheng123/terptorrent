package Logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import NetBase.ManagedConnection;
import NetBase.MessageParser.Request;
import NetBase.MessageParser.Response;
import Primitives.BitMap;
import Primitives.Piece;

/**
 * This class was constructed to make peer logic 
 * and management easier.
 * 
 * This class manages dissemination of pieces across
 * managed connections.
 * 
 * This class also maintains actively worked on pieces.
 * 
 * -Tracks which pieces are being worked on 
 * -Tracks which connections are working on which pieces
 * -Provides an interface for incoming blocks to be completed.
 * -
 * TODO: edge 1, client leaves block requeue'd but no one able to complete
 * @author wiselion
 */
public class Disseminator {
	private class ConnectionWork{
		Set<Piece> queued = new TreeSet<Piece>();
		List<Request> blockLeft = new LinkedList<Request>();//blocks queued from pieces but not yet sent.
	}
	
	//List of things disseminated
	//Managed connection.
	private Map<ManagedConnection, ConnectionWork> clientToPieceSet = new HashMap<ManagedConnection, ConnectionWork>();
	//for sake of completeness. Build it correct now so i don't have to come back
	private Map<Piece,List<ManagedConnection>> pieceToClients = new HashMap<Piece,List<ManagedConnection>>();
	private Set<Piece> currentQueue = new TreeSet<Piece>();//Queued up pieces. But none given out.
	Map<Integer,Piece> disseminatedPiecesToCompete = new HashMap<Integer,Piece>();//pieces that are given to at least 1 connection
	Map<Integer,Piece> otherPiecesGettingComplete = new HashMap<Integer,Piece>();//(why? well we don't exactly know.)
	private List<Piece> recentlyCompleted = new ArrayList<Piece>();
	
	public void connectionCleanUp(ManagedConnection mc){
		for(Piece p :clientToPieceSet.get(mc).queued){
			List<ManagedConnection> list = pieceToClients.get(p);
			list.remove(mc);
			if(list.size()==0){
				//ok remove this from every where.
				//This might be the only client that had the piece :-|
				pieceToClients.remove(p);
				disseminatedPiecesToCompete.remove(p);
				currentQueue.add(p);// no other clients working on it...
			}
		}
		clientToPieceSet.remove(mc);
	}
	
	/**
	 * This function enqueues at max "maxPieces" pieces to
	 * this connection.
	 * 
	 * Pieces are removed from the current Queue
	 * @param maxPieces
	 * @param mc
	 */
	public void enqueuePieces(int maxPieces, ManagedConnection mc){
		Iterator<Piece> itor = currentQueue.iterator();
		while(itor.hasNext()){
			Piece p = itor.next();
			if(mc.getPeerBitmap().hasPiece((int)p.pieceIndex)){
				Piece piece = disseminatedPiecesToCompete.get(p); //TO ENSURE SAME PIECE IS SHARED IN MULTI-QUE CASES
				if(piece == null){
					piece = p;
				}
				List<ManagedConnection> lMC = pieceToClients.get((int)p.pieceIndex);
				if(lMC==null){
					lMC=new ArrayList<ManagedConnection>();
				}
				lMC.add(mc);
				pieceToClients.put(p, lMC);
				ConnectionWork cw = clientToPieceSet.get(mc);
				cw.queued.add(p);
				cw.blockLeft.addAll(p.getAllBlocksLeft());
				itor.remove();
				
			}
		}
	}
	//TODO: enqueue function for pieces
	public Set<Piece> currentQueue(){
		return currentQueue;
	}
	//TODO: completion stuff.
	/**
	 * Pulls all the read in blocks.
	 * On completion cancels any mismatched sections.
	 */
	public void readFromConnection(ManagedConnection mc,BitMap b){
		List<Response> rlist = mc.getPeerResponseBlocks();
		ConnectionWork cw =  clientToPieceSet.get(mc);
		if(rlist!=null){
			for(Response r : rlist){
				if(disseminatedPiecesToCompete.containsKey(r.index)){
					System.out.println("working towards complete!");
					if(disseminatedPiecesToCompete.get(r.index).addData(r.begin, r.block)){
						//complete
						System.out.println("Complete! "+r.index);
						Piece p = disseminatedPiecesToCompete.remove(r.index);
						recentlyCompleted.add(p);
						dequeuePiece(p);
					}
				}else{
					//TODO: check if we need piece?
					if(!otherPiecesGettingComplete.containsKey(r.index)){
						otherPiecesGettingComplete.put(r.index, b.createPiece(r.index));
					}
					
					if(otherPiecesGettingComplete.get(r.index).addData(r.begin, r.block)){
						System.out.println("Complete! -Unown- "+r.index);
						recentlyCompleted.add(otherPiecesGettingComplete.remove(r.index));
					}
				}
			}
		}
	}
	
	public void initializeConnection(ManagedConnection mc){
		if(clientToPieceSet.containsKey(mc)){
			throw new RuntimeException("Incorrect use. MC already initialized");
		}
		ConnectionWork cw = new ConnectionWork();
		clientToPieceSet.put(mc,cw);
	}
	
	
	public List<Piece> recentlyCompletedPieces(){
		if(recentlyCompleted.size()==0){return null;}
		List<Piece> plist = recentlyCompleted;
		recentlyCompleted = new ArrayList<Piece>();
		return plist;
	}
	
	/**
	 * This dequeue's piece # from all clients
	 * actively working to complete.
	 * @param piece
	 */
	public void dequeuePiece(Piece p){
		List<ManagedConnection> mcs = pieceToClients.get(p);
		if(mcs !=null){
			for(ManagedConnection mc : mcs){
				ConnectionWork cw = clientToPieceSet.get(mc);
				cw.queued.remove(p);
				Iterator<Request> itor = cw.blockLeft.iterator();
				while(itor.hasNext()){
					Request r = itor.next();
					if(r.index==p.pieceIndex){
						itor.remove();
					}
				}
				mc.cancelPiece(p);
			}
		}
	}
	
}
