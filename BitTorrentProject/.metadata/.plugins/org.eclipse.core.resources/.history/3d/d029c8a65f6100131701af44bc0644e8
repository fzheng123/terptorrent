package Logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import NetBase.ManagedConnection;
import NetBase.ManagedConnection.ConnectionState;
import NetBase.MessageParser.Request;
import NetBase.MessageParser.Response;
import Primitives.BitMap.Rarity;
import Primitives.Piece;
import TorrentData.Torrent;

/**
 * This is the implementation of peer logic
 * This is probably the most basic form of a client.
 * 
 * It follows the following rules:
 * -if some one requests something that we can give:
 * 	*give it to them
 * -collect list of rarity pieces we dont have and disseminate get requests for them 
 * 
 * 
 * 
 * @author wiselion
 *
 */
public class BasicPeerLogic extends PeerLogic{
	int recvPieces = 0;
	int activeConnections =0;
	private final static int MAX_QUEUE_SIZE = 20;//TODO: 5*active connections
	private final static int NUMBER_TILL_REQUEUE=0;
	private class ConnectionWork{
		Map<Integer,Piece> piecesToComplete = new HashMap<Integer,Piece>();
		Set<Piece> piecesRequested = new HashSet<Piece>();
	}
	
	//List of pieces to be worked on
	//List of disseminated pieces
	//List of pieces that aren't disseminated yet we still received (why?)
	Map<Integer,Piece> disseminatedPiecesToCompete = new HashMap<Integer,Piece>();//pieces that are given to at least 1 connection
	Map<Integer,Piece> otherPiecesGettingComplete = new HashMap<Integer,Piece>();//(why? well we don't exactly know.)
	LinkedList<Piece> workingQueue = new LinkedList<Piece>();//Queued up pieces. 
	private Map<ManagedConnection, ConnectionWork> clientToPieceSet = new HashMap<ManagedConnection, ConnectionWork>();
	private List<Piece> recentlyCompletedPieces = new ArrayList<Piece>();
	private List<Piece> completedLast = new ArrayList<Piece>();
	private static int MAX_ENQEUED = 5;
	private static int MAX_ACTIVE_RQUESTS = 2;//too much, too little?
	
	private boolean readBlocks(Torrent t,List<Response> responses){
		//One stop shop for completetion. to keep things easy.
		if(responses==null){
			return false;
		}
		for(Response r: responses){
			if(disseminatedPiecesToCompete.containsKey(r.index)){
				if(disseminatedPiecesToCompete.get(r.index).addData(r.begin, r.block)){
					//complete
					System.out.println("Complete!");
					recentlyCompletedPieces.add(disseminatedPiecesToCompete.remove(r.index));
				}
			}else{
				//TODO: check if we need piece?
				if(!otherPiecesGettingComplete.containsKey(r.index)){
					otherPiecesGettingComplete.put(r.index, t.getBitMap().createMissingPiece(r.index));
				}
				
				if(otherPiecesGettingComplete.get(r.index).addData(r.begin, r.block)){
					System.out.println("Complete!");
					recentlyCompletedPieces.add(otherPiecesGettingComplete.remove(r.index));
				}
			}
		}		
		return true;
	}
	
	@Override
	public void doWork(Torrent t) throws IOException {
		activeConnections =0;
		Set<ManagedConnection> pList = t.getPeers();
		Iterator<ManagedConnection> itor = pList.iterator();
		while(itor.hasNext()){
			ManagedConnection p = itor.next();
			if(p.getConnectionState() == ConnectionState.uninitialized){
				p.initalizeConnection(t.getBitMap().getMapCopy());
				clientToPieceSet.put(p,new ConnectionWork());
				t.getBitMap().addPeerMap(p.getPeerBitmap());
			}else if(p.getConnectionState() == ConnectionState.closed){
				//pull off anything new?
				itor.remove();
				t.getBitMap().removePeerMap(p.getPeerBitmap());
			}else{
				p.doWork(t);
			}
			
			if(p.getConnectionState() == ConnectionState.connected){
				//Read state. 
				//Push requests (if allowed)
				//Push blocks (if allowed)
				activeConnections++;
				ConnectionWork cw = clientToPieceSet.get(p);
				if(p.amChoking()){
					p.setAmChoking(false);
				}
				if(!p.amInterested()){
					p.setAmInterested(true);
				}
				
				//if not choking:
				readBlocks(t,p.getPeerResponseBlocks());
				if(!p.peerChoking()){
					//Give them whatever the fuck they want.
					for(Request r : p.getPeerRequests()){
						if(t.pm.hasPiece(r.index)){
							Piece piece = t.pm.getPiece(r.index);
							if(piece!=null){
								p.pushRequestResponse(r, piece.getFromComplete(r.begin, r.len));
							}
						}
					}
					
					//now throw them our requests.
					//TODO: need a method to get our unawsered requests
					//something could go amiss if they aren't responding with
					//exactness. 
					//Adds Requests:
					for(Piece piece : cw.piecesToComplete.values()){
						if(cw.piecesRequested.size()>MAX_ACTIVE_RQUESTS){
							break;
						}
						if(cw.piecesRequested.contains(piece)){
							continue;
						}else{
							cw.piecesRequested.add(piece);
							List<Request> rList = piece.getAllBlocksLeft();
							for(Request r: rList){
								System.out.println("SENT REQUEST "+r.index+","+r.begin+","+r.len);
								p.pushRequest(r);
							}
						}
					}
					
					//remove completed pieces
					Iterator<Piece> pItor = cw.piecesRequested.iterator();
					while(pItor.hasNext()){
						Piece piece=pItor.next();
						if(piece.isComplete()){
							pItor.remove();
							cw.piecesRequested.remove(piece);
						}
					}
				}else{
					cw.piecesRequested.clear();//they probably dropped requests
				}
				
				if(p.peerInterested()){
					for(Piece piece : completedLast){
						p.pushHave((int) piece.pieceIndex);
					}
				}
				
			}
		}
		//TODO: something about not write values being thrown in loop
		completedLast.clear();
		completedLast.addAll(recentlyCompletedPieces);
		//TODO: makes more sense to keep bitmap in pieceManager.
		for(Piece p : recentlyCompletedPieces){
			t.pm.putPiece(p);
			t.getBitMap().addPieceComplete(p.pieceIndex);
		}
		recvPieces+=recentlyCompletedPieces.size();
		recentlyCompletedPieces.clear();
		
		//Set Completion queue by rarity.
		if(workingQueue.size() ==0 || recvPieces>NUMBER_TILL_REQUEUE){
			workingQueue.clear();
			t.getBitMap().recomputeRarity();//TODO: Dont recompute so often too cpu intensive.
			List<Rarity> rList = t.getBitMap().getRarity();
			for(Rarity rar: rList){
				if(workingQueue.size()>=MAX_QUEUE_SIZE){
					break;
				}
				//if some one has it and we don't
				if(rar.getCount()>0 && !t.getBitMap().hasPiece(rar.index)){
					workingQueue.add(t.getBitMap().createMissingPiece(rar.index));
				}
			}
		}
		
		//TODO: do something about multiple clients working on same part.
		//Dissemination from primary queue.
		Iterator<Piece> pQueue = workingQueue.iterator(); 
		while(pQueue.hasNext()){
			Piece p = pQueue.next();
			boolean taken = false;
			for(ManagedConnection mc : pList){
				if(mc.getConnectionState() == ConnectionState.connected){
					ConnectionWork cw = clientToPieceSet.get(mc);
					if(cw.piecesToComplete.size()<=MAX_ENQEUED && mc.getPeerBitmap().hasPiece((int)p.pieceIndex)){
						taken=true;
						pQueue.remove();
						if(!cw.piecesToComplete.containsKey(p.pieceIndex)){
							cw.piecesToComplete.put((int)p.pieceIndex,p);
						}
						break;
					}
				}
			}
			if(!taken){
				break;//everyone's queue'd up.
			}
		}
		
		if(System.currentTimeMillis()%1000==0){
			System.out.println("Active Connections: "+activeConnections);
		}
		t.pm.doBlockingWork();//Ughhh when do i do for this??? blocks
	}
	

}
