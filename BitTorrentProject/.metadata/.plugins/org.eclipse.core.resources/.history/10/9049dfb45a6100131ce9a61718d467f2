package TorrentData;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import NetBase.ManagedConnection;
import Primitives.BitMap;
import Primitives.DownloadFile;
import Utils.Bencoding;


/****
 * Base class. Contains all the information of the torrent
 * @author wiselion
 */
public class Torrent {
	public static int STANDARD_CACHE_SIZE = 1024*1024*20;//20MB
	private Set<ManagedConnection> peerList = new HashSet<ManagedConnection>();//TODO: keep only peers with valid connections?
	public final PieceManager pm;
	public final String peerID;
	public final int numFiles;
	public final int pieceLength;
	public final DownloadFile[] files;
	public long totalBytes;
	public final String name;
	public final byte[] byteStringHashInfo;
	public final String urlEncodedHash;
	public final String tracker;
	public final Bencoding pieceHash;
	private String trackerId;
	private long downloaded;
	private long uploaded;
	private BitMap ourMap;
	
	public Torrent(String name,int pieceLength, DownloadFile[] files,
			long totalBytes, byte[] byteStringHashInfo,String urlEncodedHash,
			String tracker,Bencoding pieceHash) {
		this.numFiles = files.length;
		this.pieceLength = pieceLength;
		this.files = files;
		this.totalBytes = totalBytes;
		this.name = name;
		this.byteStringHashInfo = byteStringHashInfo;
		this.urlEncodedHash = urlEncodedHash;
		this.tracker = tracker;
		this.announceInterval = 0;
		peerID = generateSessionKey(20);
		downloaded=0;
		uploaded=0;
		left = totalBytes;
		this.pieceHash=pieceHash;
		ourMap = new BitMap(totalBytes,pieceLength);
		try {
			pm = new PieceManager(name, files,STANDARD_CACHE_SIZE,pieceLength,ourMap.getNumberOfPieces());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("IO-Problems on piecemanager init.");
		}
	}

	public long left;//TODO: associate with files.
	public String event = "started";
	public int uPnP_Port = 1010;
	public long announceInterval; 
	
	
	public static String generateSessionKey(int length){
		String alphabet =new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"); 
		int n = alphabet.length(); 
		String result = new String(); 
		Random r = new Random(); 
		for (int i=0; i<length; i++) 
		    result = result + alphabet.charAt(r.nextInt(n));
	
		return result;
	}
	
	
	
	@Override
	public String toString(){
		return name+"\nfiles: "+numFiles+"\nSize: "+totalBytes+"\nTacker: "+tracker+"\nPeers: "+peerList.size()+"\n";
	}
	
	public void addPeer(InetAddress inet, int port,byte[] id){
		ManagedConnection mc = new ManagedConnection(inet,port,(int)pieceLength,totalBytes);
		if(id!=null){
			mc.setPreConnectionPeerID(id);
		}
		if(!peerList.contains(mc)){
			peerList.add(mc);
		}
	}
	
	public void addConnection(ManagedConnection mc){
		if(!peerList.contains(mc)){
			peerList.add(mc);
		}
	}
	
	public byte[] getInfoHash(){
		return byteStringHashInfo.clone();
	}
	
	public byte[] getPeerID(){
		try {
			return peerID.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	public long getDownloaded(){
		return downloaded;
	}
	
	public long getUploaded(){
		return uploaded;
	}
	
	public void addDownloaded(long bytes){
		downloaded+=bytes;
	}
	
	public void addUploaded(long bytes){
		downloaded+=bytes;
	}
	
	public long getLeft(){
		left =(totalBytes-pm.getCompletedBytes());
		left = left>0?left:0;
		return left;
	}



	public Set<ManagedConnection> getPeers() {
		return peerList;
	}
	
	public BitMap getBitMap(){
		return ourMap;
	}
	

	
}
