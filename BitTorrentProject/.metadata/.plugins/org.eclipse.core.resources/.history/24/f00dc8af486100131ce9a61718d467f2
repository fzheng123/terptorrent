package NetBase;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import Main.UnitTests;
import NetBase.MessageParser.HandShake;
import NetBase.MessageParser.PeerMessage;
import NetBase.MessageParser.Request;
import NetBase.MessageParser.Response;
import Primitives.BitMap;
import TorrentData.Torrent;
import Utils.TorrentParser;

/***
 * Let this be the managedConnection for the peer
 * Let it handle things like communication
 * Let this be the Socket of the connection
 * 
 * Put simply managedconnection will follow the rules of the protocol.
 * It will take in and attempt to push out data as is available by the rules.
 * This class is very much the state of the connection.
 * TODO: announced map control for under reporting?
 * We store long because 4 byte UNSIGNED indexing is used.
 * 
 * 
 * By the specs:
 * 
 * CHOKED: 
 *  When a peer chokes the client, it is a notification that no requests will be answered until the client is unchoked. 
 *  The client should not attempt to send requests for blocks, and it should consider all pending (unanswered) requests to be discarded by the remote peer.
 *  -may send have (if peer interested)
 *  -may send blocks (if we aren't peer not choked)
 *  
 * Interested:
 *  Whether or not the remote peer is interested in something this client has to offer.
 *  This is a notification that the remote peer will begin requesting blocks when the client unchokes them.
 *  -may send blocks(if we aren't peer not choked)
 * 	-may send requests (if peer don't have us choked)
 *  
 *  This class composed of 90% boiler plate for good api interface.
 *  @Warning: 2MB output stream is set. If this gets fully utilized it will block.
 *  //TODO: make block safe. boolean perhaps?
 */
public class ManagedConnection {
	/***
	 * State of this connection. 
	 * uninitialized -> meaning no connection attempt made
	 * closed -> meaning socket disconnect, was requested, or error occured.
	 * pending -> socket connected but handshake not yet completed.
	 */
	public static enum ConnectionState{
		uninitialized,
		pending,
		connected,
		closed
	}
	
	private ConnectionState conState;
	private int port;
	private InetAddress ip;
	private boolean connectionInit;
	private boolean sentHandShake;
	private Socket sock;
	//Requests fromUs
	//Requests fromPeer
	private Set<Request> ourRequests; //from us
	private Set<Request> peerRequests;  //from peer
	
	//Normally we might do this
	//esp. if we want bandwidth control!
	//leave out for now:
	//Blocks fromPeer?
	//Blocks fromUs?
	private List<Response> peerSentBlocks; 
	private HandShake hs;
	
	private MessageParser mp;
	private boolean am_choking = true;
	private boolean am_interested = false;
	private boolean peer_choking =true;
	private boolean peer_interested =false;
	private long download;
	private long upload;
	private long maintenance;
	private BitMap peerBitMap;
	private OutputStream sockOut;
	private InputStream sockIn;
	private byte [] announcedMap;//Note this is probably a clone.
	
	public ManagedConnection(InetAddress ip,int port,int pieceLenght, long totalBytes){
		this.ip=ip;
		this.port=port;
		peerBitMap = new BitMap(totalBytes,pieceLenght);
		download = upload = maintenance = 0;
		mp = new MessageParser();
		peerRequests = new HashSet<Request>();
		ourRequests = new HashSet<Request>();
		peerSentBlocks = new ArrayList<Response>();
		connectionInit = false;
		conState = ConnectionState.uninitialized;
		
	}
	
	/**
	 * Takes in the byte map for announcing
	 * @param announcedMap
	 */
	public void initalizeConnection(byte [] announcedMap){
		if(sock != null){
			throw new RuntimeException("Connection already initialized");
		}
		

			sock = new Socket();
			conState = ConnectionState.pending;
		//Threading the connect phase.
		//TODO: Check conventions if this is ok.
		new Thread(){
			@Override
			public void run(){
				try {
					sock.setSoTimeout(2*60*1000);//2 min timeout
					sock.setReceiveBufferSize(1024*1024*2);
					sock.setSendBufferSize(1024*1024*2);
					sock.connect(new InetSocketAddress(ip, port));//BLOCKS!
					if(sock.isConnected()){
						conState = ConnectionState.connected;
					}else{
						//should have thrown error.
						conState = ConnectionState.closed;
					}
				} catch (IOException e) {
					e.printStackTrace();
					conState = ConnectionState.closed;
				}
			}
		}.start();
		
		
		if(announcedMap==null || announcedMap.length!=peerBitMap.getLength()){
			throw new RuntimeException("Invalid announced map. Either null or incorrect length!");
		}
		
		this.announcedMap=announcedMap;
	}
	
	/**
	 * Only a couple of rules here.
	 * DONT BLOCK.
	 * @throws IOException 
	 */
	public void doWork(Torrent t) throws IOException{
		if(conState == ConnectionState.closed || conState==ConnectionState.uninitialized){
			throw new RuntimeException("Invalid request. Cant do work on closed/uninitialized connections");
		}
		
		if(conState == ConnectionState.pending){
			//Do nothing till connected state.
			return;
		}
		if(conState== ConnectionState.connected){
			
		}
		
		
		try{
			if(!connectionInit){
				if(!sentHandShake){
					sentHandShake=true;
					mp.sendHandShake(sockOut, t.getInfoHash(), t.getPeerID());
					System.out.println("Sent hand shake");
				}
				hs = mp.readHandShake(sockIn);
				if(hs!=null){
					
					//send bitmap
					if(!Arrays.equals(hs.hashInfo, t.getInfoHash())){
						System.out.println("INFO HASH DONT MATCH!");
						conState = ConnectionState.closed;
						sock.close();
						return;
					}else{
						conState = ConnectionState.connected;
					}
					mp.bitfield(sockOut, announcedMap);
					connectionInit = true;
					System.out.println("GOT HAND SHAKE!");
				}
			}else{
				mp.readMessage(sockIn);
				while(mp.hasMessage()){
					//Input logic.
					PeerMessage pm = mp.getNext();
					doDataIn(pm,t);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			conState=ConnectionState.closed;
		}
	}
	
	
	//State Getters
	public BitMap getPeerBitmap(){
		if(conState != ConnectionState.uninitialized && conState != ConnectionState.pending){
			return peerBitMap;
		}
		throw new RuntimeException("Wont give you bitmap in a non yet active connection!");
	}
	
	public boolean amChoking(){
		return am_choking;
	}
	
	public boolean amInterested(){
		return am_interested;
	}
	
	public boolean peerChoking(){
		return peer_choking;
	}
	
	public boolean peerInterested(){
		return peer_interested;
	}
	
	/***
	 * Immediate IO!
	 * Wont block!
	 * If set to choke we drop their request list!
	 * @param t
	 */
	public void setAmChoking(boolean t){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send choke state on 'connected' connections");
		}
		try {
			if (am_choking != t) {
				if(t) {
					mp.choke(sockOut);
					peerRequests.clear();
				} else {
					mp.unchoke(sockOut);
				}
				am_choking = t;
				maintenance+=5;
			}
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
	}
	
	public void setAmInterested(boolean t){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send choke on 'connected' connections");
		}
		try {
			
			if (am_interested != t) {
				if (t) {
					mp.interested(sockOut);
					
				} else {
					mp.not_interested(sockOut);
				}
				maintenance+=5;
				am_interested = t;
			}
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
	}
	
	/***
	 * Adds a request for block.
	 * If requests already exists will do
	 * nothing.
	 * These are hard sends! [data will be written on call]
	 * The upload and maintenance counter will be updated.
	 * @param r
	 */
	public boolean pushRequest(Request r){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send requests on 'connected' connections");
		}else if(peer_choking){
			throw new RuntimeException("Can only send requests on unchoked connections");
		}
		if(ourRequests.contains(r.index)){
			return false;//already contained.
		}
		ourRequests.add(r);
		try {
			maintenance+=17;
			mp.request(sockOut, r.index,r.begin,r.len);
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
		return true;
	}
	
	/***
	 * Sends a cancel for given block request.
	 * If remove doesn't match previous add request error will
	 * be thrown. 
	 * Note: requests are removed on receive of block
	 * These are hard sends! [data will be written on call]
	 * The upload and maintenance counter will be updated.
	 * @param r
	 */
	public void pushCancel(Request r){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send requests on 'connected' connections");
		}else if(peer_choking){
			throw new RuntimeException("Can only send requests on unchoked connections");
		}else if(!ourRequests.contains(r)){
			throw new RuntimeException("Can't canel request that doesn't exist!");
		}
		ourRequests.remove(r);
		try {
			maintenance+=17;
			mp.cancel(sockOut, r.index,r.begin,r.len);
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
		
	}
	
	/**
	 * Takes block response, will throw error if response doesnt
	 * exist in peerRequest set.
	 * These are hard sends! [data will be written on call]
	 * The upload and maintenance counter will be updated.
	 * @param r
	 * @param block
	 */
	public void pushRequestResponse(Request r, byte[] block){
		if(!peerRequests.remove(r)){
			throw new RuntimeException("Giving unrequested block! But whyyyy! =(");
		}else if(conState != ConnectionState.connected){
			throw new RuntimeException("This is invalid connection state isn't in connected mode!");
		}else if(peer_choking){
			throw new RuntimeException("We are being choked! Not valid to send silly!");
		}
		try {
			maintenance+=13;
			upload += block.length;
			mp.piece(sockOut, r.index,r.begin,block);
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
	}
	
	public void pushHave(int index){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send have on 'connected' connections");
		}else if(!peer_interested){
			throw new RuntimeException("Can only send have on interested connections");
		}
		try {
			maintenance+=10;
			mp.have(sockOut, index);
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
	}
	
	
	/**
	 * Returns list of active requests.
	 * @author wiselion
	 * @return
	 */
	public List<Request> getPeerRequests(){
		List<Request> q =new ArrayList<Request>(peerRequests.size());
		for(Request r:peerRequests){
			q.add(r);
		}
		return q;
	}
	
	/**
	 * List of blocks gotten since last call.
	 * Note will return null if size 0. 
	 * @return
	 */
	public List<Response> getPeerResponseBlocks(){
		if(peerSentBlocks.size()<1){return null;}
		List<Response> r = peerSentBlocks;
		peerSentBlocks = new ArrayList<Response>();
		return r;
	}
	
	public void cancelPieceRequest(int index) throws IOException{
		if(peer_choking){
			return;//we wont do anything.
		}else if(conState != ConnectionState.connected){
			throw new RuntimeException("Can't canel request on unconnected");
		}
		
		Iterator<Request> rq = ourRequests.iterator();
		while(rq.hasNext()){
			Request r = rq.next();
			if(r.index==index){
				//send cancel
				maintenance+=17;
				mp.cancel(sockOut, r.index,r.begin,r.len);
				rq.remove();
			}
		}
	}
	
	
	public long getDownloadedBytes(){
		return download;
	}
	
	public long getUploadedBytes(){
		return upload;
	}
	
	public long getManagementBytes(){
		return maintenance;
	}
	
	public ConnectionState getConnectionState(){
		return conState;
	}
	
	private void doDataIn(PeerMessage pm, Torrent torrent){
		if(pm.type == PeerMessage.Type.CHOKE){
			this.peer_choking = true;
			//Drop our requests. They aint gana get done.
			System.out.println(toString()+" choked us.");
			ourRequests.clear();
		}else if(pm.type == PeerMessage.Type.UNCHOKE){
			System.out.println(toString()+" unchoked us.");
			this.peer_choking = false;
		}else if(pm.type == PeerMessage.Type.INTERESTED){
			System.out.println(toString()+" intrested in us.");
			this.peer_interested = true;
		}else if(pm.type == PeerMessage.Type.NOT_INTERESTED){
			System.out.println(toString()+" not intrested in us.");
			this.peer_interested = false;
		}else if(pm.type == PeerMessage.Type.HAVE){
			peerBitMap.addPieceComplete(pm.piece);
			if(!am_interested){
				System.out.println("Client sent us have! But WE AIN'T EVEN interested.");
			}
		}else if(pm.type == PeerMessage.Type.BIT_FILED){
			peerBitMap.setBitMap(pm.bitfield);
		}else if(pm.type == PeerMessage.Type.REQUEST){
			peerRequests.add(new Request(pm.index,pm.begin,pm.length));
			if(am_choking){
				System.out.println("Recieved request but choking request!");
			}
			
		}else if(pm.type == PeerMessage.Type.CANCEL){
			peerRequests.remove(new Request(pm.index,pm.begin,pm.length));//should work
		}else if(pm.type == PeerMessage.Type.PIECE){
			//get piece
			Request r = new Request(pm.index,pm.begin,pm.block.length);
			Response rs = new Response(pm.index,pm.begin,pm.block);
			if(ourRequests.remove(r)){
				download+= pm.block.length;
			}else{
				//For now this isnt a shutdownable event.
				System.out.println("Recieved Piece "+pm.index+","+pm.begin+","+pm.length+" but didnt send request!");
			}
			peerSentBlocks.add(rs);
		}
	}
	
	public void shutDown() throws IOException{
		sock.close();
		conState = ConnectionState.closed;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof ManagedConnection){
			ManagedConnection m = (ManagedConnection) o;
			return m.ip.equals(ip)&&m.port==port;
		}
		return false;
	}
	
	//TODO: simple read write test with 2 managed connections with different io steams:
	public static final void testManagedConnection() throws IOException, NoSuchAlgorithmException{
		//primitive test. pipe 1 stream into another.
		//See if they can talk to each other.
		Torrent t = TorrentParser.parseTorrentFile("ubuntu.torrent");
		ManagedConnection m1 = new ManagedConnection(null,-1,t.pieceLength,t.totalBytes);
		ManagedConnection m2 = new ManagedConnection(null,-1,t.pieceLength,t.totalBytes);
		m1.announcedMap = t.getBitMap().getMapCopy();
		m2.announcedMap = t.getBitMap().getMapCopy();
		m2.conState=m1.conState = ConnectionState.pending;//we aint actually connecting to anything!
		//Bridge the streams
		PipedInputStream in = new PipedInputStream(1024*1024*4);
		PipedOutputStream out = new PipedOutputStream(in);
		//m1 -> m2
		m1.sockOut = out;
		m2.sockIn = in;
		//m2 -> m1
		in = new PipedInputStream(1024*1024*4);
		out = new PipedOutputStream(in);
		m1.sockIn = in;
		m2.sockOut = out;
		//Alright time to get a testing!
		m1.doWork(t);
		m2.doWork(t);
		//m2 should be connected.
		System.out.println("	Test 1: "+(m1.conState == ConnectionState.pending&&m2.conState==ConnectionState.connected));
		m1.doWork(t);
		//m1 shoudl be connected.
		System.out.println("	Test 2: "+(m1.conState == ConnectionState.connected&&m2.conState==ConnectionState.connected));
		
		//TODO: Choke-unchoke tests in both directions, Interested tests in both directions
		m1.setAmChoking(false);
		m1.setAmInterested(true);
		m1.doWork(t);
		m2.doWork(t);
		System.out.println("	Test 3: "+(m2.amChoking() &&!m2.amInterested()&&!m2.peerChoking()&&m2.peerInterested()));
		m2.setAmChoking(false);
		m2.setAmInterested(true);
		m1.doWork(t);
		System.out.println("	Test 4: "+(!m2.amChoking() &&m2.amInterested()&&!m1.peerChoking()&&m1.peerInterested()));
		
		//Do Requests
		m1.pushHave(1024);
		m2.pushHave(1025);
		m1.pushHave(1020);
		m2.pushHave(1022);
		m1.pushHave(1023);
		m2.pushHave(1026);
		m1.doWork(t);
		m2.doWork(t);
		BitMap bm1 = m1.getPeerBitmap();
		BitMap bm2 = m2.getPeerBitmap();
		//bm1 = m2.pushHave
		//bm2 = m1.pushHave
		boolean b = bm1.hasPiece(1025)&&bm1.hasPiece(1022)&&bm1.hasPiece(1026);
		boolean c = bm2.hasPiece(1024)&&bm2.hasPiece(1020)&&bm2.hasPiece(1023);
		System.out.println("	Test 5: "+(b && c));
		//Do requests
		m1.pushRequest(new Request(1025,0,1024));
		m2.pushRequest(new Request(1024,0,1024));
		m1.pushRequest(new Request(1022,0,1024));
		m2.pushRequest(new Request(1020,0,1024));
		m1.doWork(t);
		m2.doWork(t);
		List<Request> lr1 = m1.getPeerRequests();
		List<Request> lr2 = m2.getPeerRequests();
		System.out.println("	Test 6: "+(lr1.contains(new Request(1024,0,1024))&&lr1.contains(new Request(1020,0,1024))&&
				lr2.contains(new Request(1025,0,1024))&&lr2.contains(new Request(1022,0,1024))));
		//Do block pushes
		byte[] block1=UnitTests.testGen(1024);
		byte[] block2=UnitTests.testGen(1024);
		m1.pushRequestResponse(lr1.get(0), block1);
		m2.pushRequestResponse(lr2.get(0), block2);
		m1.doWork(t);
		m2.doWork(t);
		b = Arrays.equals(m1.getPeerResponseBlocks().get(0).block,block2);
		c = Arrays.equals(m2.getPeerResponseBlocks().get(0).block,block1);
		System.out.println("	Test 7: "+(b && c));
		System.out.println("	Test 8:"+!m1.getPeerRequests().contains(lr1.get(0)));
		//Do cancels
		m1.pushCancel(new Request(1022,0,1024));
		m2.pushCancel(new Request(1020,0,1024));
		m1.doWork(t);
		m2.doWork(t);
		b = !m1.getPeerRequests().contains(new Request(1020,0,1024));
		c = !m2.getPeerRequests().contains(new Request(1022,0,1024));
		System.out.println("	Test 9 : "+(b && c));
	}
	
	@Override
	public String toString(){
		if(hs!=null){
			BigInteger bi = new BigInteger(1, hs.peerID);
		    return String.format("%0" + ( hs.peerID.length << 1) + "X", bi);
		}else{
			//no hand shake just refer to as ip and port.
			return (ip.toString()+":"+port);
		}
	}
	
}
