package TorrentData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import Primitives.BitMap;
import Primitives.DownloadFile;
import Primitives.Piece;

/**
 * Class that manages files and pieces.
 * Should manage piece and file interactions.
 * Cache-ing?
 * 
 * Working with big files can get complicated....
 * #files = pieces / 3k 
 * for reference # pieces for huge torrent[120GB with 64KB piece]:
 * ~1.8 million pieces
 * ~600 files
 * Ok so that bad. Instead we will keep map
 * of piece to file offset. This will have to be enough.
 * 
 * Goal here is to beable to fit on 256MB of ram fyi.
 * Maps in worse case[1.8 mil] will use 6.2% of this
 * 
 * FileFormat:
 * <piece index(4)><piece data(pieceLength)>
 * Why the extra 4 bytes?
 * What if we only partially download, thats why.
 * Our cache is LRU cahce 
 * from LinkedHashMap java doc:
 * "This kind of map is well-suited to building LRU caches."
 * :-) ^.^ ^_^ O_o !! woot. 
 */
public class PieceManager {
	//TODO: private Set<Long> skippedPieces;, actually should keeps chunks of this.
	Set<Long> completedPieces;
	Map<Long,Long> pieceToFileOffSet;//Yeah. Its that bad. .05% usage
	LinkedHashMap<Long,Piece> indexToPiece;//Subset of completed pieces, quick LRU cache
	List<Piece> writeToDisk;//set that still needs to be written to disk.
	List<Long> readsFromDisk;
	int cacheSize;
	int pieceLength;
	DownloadFile [] files;
	File partialFile;
	RandomAccessFile raf;
	public final BitMap bitmap;
	/**
	 * 
	 * @param folderPath
	 * @param files
	 * @param cacheSize
	 * @param pieceLength
	 * @param pieces
	 * @throws FileNotFoundException
	 */
	public PieceManager(String folderPath,DownloadFile [] files,int cacheBytes,int pieceLength,long totalBytes)
			throws FileNotFoundException{
	
		int actualChunks = cacheBytes/pieceLength;
		if(actualChunks < 2){
			actualChunks=2;
		}
		
		
		bitmap = new BitMap(totalBytes,pieceLength);
		completedPieces = new HashSet<Long>(bitmap.getNumberOfPieces());	
		writeToDisk = new ArrayList<Piece>();
		readsFromDisk = new ArrayList<Long>();
		
		this.cacheSize = actualChunks;
		this.files = files;
		partialFile = new File(folderPath+"/partialDownload.Partial");
		//make sure the dirs are there.
		if(partialFile.getParent()!=null || partialFile.getParent().equals("")){
			File dir = new File(partialFile.getParent());
			dir.mkdirs();
		}
		boolean readOut = false;
		if(partialFile.isFile()){
			//read for opening
			readOut = true;
		}
		
		this.pieceLength =pieceLength;
		pieceToFileOffSet = new HashMap<Long,Long>(actualChunks+2,1);
		//Java's pretty awesome....
		indexToPiece = new LinkedHashMap<Long,Piece>(actualChunks+2,1,true){//true->access order
			private static final long serialVersionUID = -1418159831489090492L;
			protected boolean removeEldestEntry(Map.Entry<Long,Piece> eldest) {
	            return size() > cacheSize;
	         }
		};
		raf= new RandomAccessFile(partialFile,"rw");
	}
	
	/**
	 * Returns true if has the piece.
	 * @param pieceIndex
	 * @return
	 */
	public boolean hasPiece(int pieceIndex){
		return bitmap.hasPiece(pieceIndex);
	}
	
	/***
	 * @param p-  a new piece to be placed into structure
	 * @return false if it already existed.
	 * May change to throw error... I dunno.
	 * 
	 */
	public boolean putPiece(Piece p){
		indexToPiece.put(p.pieceIndex,p);
		//"If this set already contains the element, the call leaves the set unchanged and returns false."
		boolean b= completedPieces.add(p.pieceIndex);
		//If this set already contains the element, the call leaves the set unchanged and returns false.
		if(b){
			bitmap.addPieceComplete(p.pieceIndex);
			writeToDisk.add(p);
		}
		return b;
	}
	
	
	/***
	 * Gets piece from structure.
	 * May have to read from disk.
	 * Assume hasPiece is called before getPiece
	 * Throws error if piece not completed
	 * Will return null when not loaded on cahce.
	 * It will be enqueued for cache if returns null.
	 * @param pieceIndex
	 * @return
	 */
	public Piece getPiece(long pieceIndex){
		//get piece must do puts so we can keep
		//stuff being used at top
		if(!completedPieces.contains(pieceIndex)){
			throw new RuntimeException("Cant get piece that hasnt been completed.");
		}
		if(indexToPiece.containsKey(pieceIndex)){
			return indexToPiece.get(pieceIndex);
		}
		readsFromDisk.add(pieceIndex);
		return null;
	}
	
	
	/**
	 * Requests that we couldn't immediately do
	 * get put into a block queue.
	 * @throws IOException 
	 */
	public void doBlockingWork() throws IOException{
		//do all our writes and reads here.
		//for now we will store into our own .Partial format
		//Write then open for read
		//RandomAccessFile raf = new RandomAccessFile(partialFile,"rw");
		//Awesome ^.^ -> raf.getFilePointer()
		for(Piece p:writeToDisk){
			pieceToFileOffSet.put(p.pieceIndex,raf.getFilePointer());
			System.out.println(""+raf.getFilePointer());
			raf.writeLong(p.pieceIndex);
			System.out.println(""+raf.getFilePointer());
			raf.write(p.getCompleted());
		}
		
		for(Long pIndex:readsFromDisk){
			long off = pieceToFileOffSet.get(pIndex);
			raf.seek(off+4);
			byte [] p = new byte[pieceLength];
			raf.read(p);
			Piece piece = new Piece(pIndex,p);
			indexToPiece.put(piece.pieceIndex, piece);
		}
		
		//Our work here is done.
		writeToDisk.clear();
		readsFromDisk.clear();
	}
	
	/**
	 * Blocks, But garentee's non-null return.
	 * @return
	 * @throws IOException 
	 */
	public Piece getAbsolutePiece(long pieceIndex) throws IOException{
		
		if(indexToPiece.containsKey(pieceIndex)){
			return indexToPiece.get(pieceIndex);
		}else{
			
			long off = pieceToFileOffSet.get(pieceIndex);
			raf.seek(off+4);
			byte [] p = new byte[pieceLength];
			raf.read(p);
			Piece piece = new Piece(pieceIndex,p);
			return piece;
		}
	}
	
	/***
	 * Ideally we wouldn't need this.
	 * But for now this is how we get our our files.
	 * @throws IOException 
	 */
	public void doCompletionPhase() throws IOException{
		//for each file....
		doBlockingWork();
		raf.getChannel().force(true);
		for(DownloadFile f: files){
			//check files has all pieces complete.
			long off = f.getOffSet();
			long l = ((f.getOffSet()+f.getLength())%pieceLength);
			if(l==0){
				l=pieceLength;
			}
			long pStart = off/pieceLength;
			long pEnd =(long)Math.ceil((double)(off+f.getLength())/pieceLength);
			for(long s=pStart;s<pEnd;s++){
				if(!completedPieces.contains(s)){
					System.out.println("File not complete! Index "+s+" missing!");
				}
			}
			File file =f.getFileHandle();
			if(file.getParent()!=null){//Make dir's if needed
				File dir = new File(file.getParent());
				dir.mkdirs();
			}
			RandomAccessFile raf = new RandomAccessFile(file,"rw");
			raf.seek(0);
			for(long s=pStart;s<pEnd;s++){
				//now we start writing ^.^
				Piece p =getAbsolutePiece(s);
				byte [] b =p.getCompleted();
				if(s==pStart){
					long start = off%(long)pieceLength;
					raf.write(b,(int)start,(int)(b.length-start));
				}else if(s==pEnd-1){
					long end = l;
					raf.write(b,(int)0,(int)(end));
				}else{
					raf.write(b);
				}
			}
			raf.close();
		}
	}
	
	public long getCompletedBytes(){
		return completedPieces.size()*pieceLength;
	}

	
	private static final Random r = new Random(); 
	public static byte[] generateSessionKey(int length) throws UnsupportedEncodingException{
		String alphabet =new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"); 
		int n = alphabet.length(); 
		String result = new String(); 
		for (int i=0; i<length; i++) 
		    result = result + alphabet.charAt(r.nextInt(n));
	
		return result.getBytes("UTF-8");
	}
	
	public static void main(String[]args) throws IOException{
		//Do test:
		//Simple test load up 16KB of something random
		//set output for 2 files
		//Check file and make sure everything is correct
		DownloadFile [] files = new DownloadFile[2];
		files[0]=new DownloadFile("folderTest","file1.txt",8*1024l,0);
		files[1]=new DownloadFile("folderTest","file2.txt",8*1024l,8*1024l);
		int pieceSize = 1000;//Note these files are going to fall on a piece gap.
		long totalSize = 8*1024l +8*1024l;
		long pieces = (long)Math.ceil((double)totalSize/pieceSize);
		PieceManager pm = new PieceManager("folderTest", files, 2000, pieceSize,totalSize);
		List<Piece> piecesActual = new ArrayList<Piece>();
		for(long i =0;i<=pieces;i++){//17 pieces. makes sense...
			byte [] b =generateSessionKey(1000);
			Piece p = new Piece(i,b);
			piecesActual.add(p);
			pm.putPiece(p);
			if(i==2){
				if(pm.getPiece(0)==null){
					System.out.print("Caching might work!");
				}
				pm.doBlockingWork();
				if(pm.getPiece(0)!=null){
					System.out.print("Caching works!");
				}
			}
		}
		pm.doBlockingWork();
		pm.doCompletionPhase();
		//Out put should be 2 files that are exactly 2KB
	}
	
	
	
}
