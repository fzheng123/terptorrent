package Primitives;

import java.util.ArrayList;
import java.util.List;

/***
 * Primitive data structure used to determine:
 * 1. Optimal pieces to get (rarity)
 * 2. A sharable data structure that is immutable.
 * MAX PEERS: 32k
 */
public class BitMap {
	byte [] ourBitMap;
	short [] rarityMap;
	int pieceLength;
	long totalData;
	int numPieces;
	List<BitMap> peerMaps;

	public BitMap(long totalData, int pieceLength){
		numPieces = (int) Math.ceil(totalData/pieceLength);
		ourBitMap = new byte[(int)Math.ceil(numPieces/8.0)];
		peerMaps = new ArrayList<BitMap>();
		
	}
	
	public int getNumberOfPieces(){
		return numPieces;
	}
	
	public long getTotalSize(){
		return totalData;
	}
	
	public void addPeerMap(BitMap bm){
		peerMaps.add(bm);
	}
	
	public boolean removePeerMap(BitMap bm){
		return peerMaps.remove(bm);
	}
	
	public void addPieceComplete(long i){
		int index = (int) i;
		byte  b= ourBitMap[index/8];
		b = (byte) (b | (1 << index%8));
		ourBitMap[index/8] = b;
	}
	
	public boolean hasPiece(int index){
		return (ourBitMap[index/8] & (1 << index%8)) == 1;
	}
	
	/***
	 * O(peers*pieces)
	 * Grows linearly with number of peers
	 */
	public void recomputeRarity(){
		for(int i=0;i<rarityMap.length;i++){
			rarityMap[i]=0;
		}
		for(int i=0;i<numPieces;i++){
			for(BitMap b: peerMaps){
				rarityMap[i] +=b.hasPiece(i)?1:0;
			}
		}
	}
	
	public void setBitMap(byte [] bitmap){
		if(bitmap.length != this.ourBitMap.length){
			throw new RuntimeException("invalid bitmap length");
		}
		for(int i=0;i<bitmap.length;i++){
			this.ourBitMap[i]=bitmap[i];
		}
	}
	
	
	
}
