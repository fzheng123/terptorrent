package NetBase;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import NetBase.MessageParser.HandShake;
import NetBase.MessageParser.PeerMessage;
import NetBase.MessageParser.Request;
import NetBase.MessageParser.Response;
import Primitives.BitMap;
import Primitives.Piece;
import TorrentData.Torrent;

/***
 * Let this be the managedConnection for the peer
 * Let it handle things like communication
 * Let this be the Socket of the connection
 * 
 * Put simply managedconnection will follow the rules of the protocol.
 * It will take in and attempt to push out data as is available by the rules.
 * This class is very much the state of the connection.
 * TODO: announced map control for under reporting?
 * We store long because 4 byte UNSIGNED indexing is used.
 * 
 * 
 * By the specs:
 * 
 * CHOKED: 
 *  When a peer chokes the client, it is a notification that no requests will be answered until the client is unchoked. 
 *  The client should not attempt to send requests for blocks, and it should consider all pending (unanswered) requests to be discarded by the remote peer.
 *  -may send have (if peer interested)
 *  -may send blocks (if we aren't peer not choked)
 *  
 * Interested:
 *  Whether or not the remote peer is interested in something this client has to offer.
 *  This is a notification that the remote peer will begin requesting blocks when the client unchokes them.
 *  -may send blocks(if we aren't peer not choked)
 * 	-may send requests (if peer don't have us choked)
 *  
 */
public class ManagedConnection {
	/***
	 * State of this connection. 
	 * uninitialized -> meaning no connection attempt made
	 * closed -> meaning socket disconnect, was requested, or error occured.
	 * pending -> socket connected but handshake not yet completed.
	 */
	public static enum ConnectionState{
		uninitialized,
		pending,
		connected,
		closed
	}
	
	ConnectionState conState;
	final static int MAX_BUFFER = 5;//TODO: let max buffer grow like slow start?
	final static int MAX_DEPT = -16*1024*3;
	final static int Active_Request =1;//max # of requests in flight
	private int port;
	private InetAddress ip;
	private boolean connectionInit;
	private boolean sentHandShake;
	private Socket sock;
	
	//Requests fromUs
	//Requests fromPeer
	private Set<Request> ourRequests; //from us
	private Set<Request> peerRequests;  //from peer
	
	//Normally we might do this
	//esp. if we want bandwidth control!
	//leave out for now:
	//Blocks fromPeer?
	//Blocks fromUs?
	private List<Response> peerSentBlocks; 
	
	
	private MessageParser mp;
	private boolean am_choking = true;
	private boolean am_interested = false;
	private boolean peer_choking =true;
	private boolean peer_interested =false;
	private long download;
	private long upload;
	private long maintenance;
	private BitMap peerBitMap;
	private OutputStream sockOut;
	private InputStream sockIn;
	
	
	public ManagedConnection(InetAddress ip,int port,int pieceLenght, int totalBytes){
		this.ip=ip;
		this.port=port;
		peerBitMap = new BitMap(totalBytes,pieceLenght);
		download = upload = maintenance = 0;
		mp = new MessageParser();
		peerRequests = new HashSet<Request>();
		ourRequests = new HashSet<Request>();
		connectionInit = false;
		conState = ConnectionState.uninitialized;
	}
	
	public void initalizeConnection(){
		if(sock == null){
			throw new RuntimeException("Connection already initialized");
		}
		try {
			sock= new Socket(ip, port);
			conState = ConnectionState.pending;
		} catch (IOException e) {
			e.printStackTrace();
			conState = ConnectionState.connected;
		}
	}
	
	/**
	 * Only a couple of rules here.
	 * DONT BLOCK.
	 * @throws IOException 
	 */

	public void doWork(Torrent t) throws IOException{
		//Shouldn't happen.
		if(conState == ConnectionState.closed || conState==ConnectionState.uninitialized){
			throw new RuntimeException("Invalid request. Cant do work on closed/uninitialized connections");
		}
		
		
		if(!connectionInit){
			if(!sentHandShake){
				sentHandShake=true;
				mp.sendHandShake(sockOut, t.getInfoHash(), t.getPeerID());
			}
			HandShake hs = mp.readHandShake(sockIn);
			if(hs!=null){
				connectionInit = true;
				//send bitmap
				if(!Arrays.equals(hs.hashInfo, t.getInfoHash())){
					System.out.println("INFO HASH DONT MATCH!");
					conState = ConnectionState.closed;
					sock.close();
					return;
				}
				mp.bitfield(sockOut, t.getBitMap());
			}
		}else{
			mp.readMessage(sockIn);
			while(mp.hasMessage()){
				//Input logic.
				PeerMessage pm = mp.getNext();
				doDataIn(pm,t);
			}
			
			//OutBound logic:
			//See Section OUTBOUND-LOGIC
			if(!am_interested){
				//Were always interested!
				am_interested = true;
				mp.interested(sockOut);
			}
			
			if(am_choking && dataRecieved-dataSent>MAX_DEPT){
				am_choking = false;
				mp.unchoke(sockOut);
			}
			
			if(!peer_choking){
				//Lets do fun stuff?
				if(ourRequests.isEmpty()){
					for(Long l : bufferedPieces.keySet()){
						Piece p = bufferedPieces.get("l");
						ourRequests.add(p.getNextBlock());
						break;
					}
				}
				for(Request r : ourRequests){
					if(!r.sent){
						mp.request(sockOut, r.index, r.begin, r.len);
					}
				}
				
				//Lets send any active requests
				for(Request r : peerRequests){
					Piece p = t.getPiece(r.index);
					if(p!=null){
						//sends block of completed piece 
						mp.piece(sockOut, r.index, r.begin, p.getFromComplete(r.begin,r.len));
					}
				}
				//if up versedown do something?
			}
			
			if(peer_interested){
				//Do something.
			}
		}
		
		
	}
	
	
	//State Getters
	public BitMap getPeerBitmap(){
		if(conState != ConnectionState.uninitialized && conState != ConnectionState.pending){
			return peerBitMap;
		}
		throw new RuntimeException("Wont give you bitmap in a non yet active connection!");
	}
	
	public boolean amChoking(){
		return am_choking;
	}
	
	public boolean amInterested(){
		return am_interested;
	}
	
	/***
	 * Immediate IO!
	 * Wont block!
	 * If set to choke we drop their request list!
	 * @param t
	 */
	public void setAmChoking(boolean t){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send choke state on 'connected' connections");
		}
		try {
			if (am_choking != t) {
				if(t) {
					mp.choke(sockOut);
					peerRequests.clear();
				} else {
					mp.unchoke(sockOut);
				}
				am_choking = t;
				maintenance+=5;
			}
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
	}
	
	public void setAmInterested(boolean t){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send choke on 'connected' connections");
		}
		try {
			
			if (am_interested != t) {
				if (t) {
					mp.interested(sockOut);
					
				} else {
					mp.not_interested(sockOut);
				}
				maintenance+=5;
				am_interested = t;
			}
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
	}
	
	/***
	 * Adds a request for block.
	 * If requests already exists will do
	 * nothing.
	 * These are hard sends! [data will be written on call]
	 * The upload and maintenance counter will be updated.
	 * @param r
	 */
	public boolean addRequest(Request r){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send requests on 'connected' connections");
		}else if(peer_choking){
			throw new RuntimeException("Can only send requests on unchoked connections");
		}
		if(ourRequests.contains(r.index)){
			return false;//already contained.
		}
		ourRequests.add(r);
		try {
			maintenance+=17;
			mp.request(sockOut, r.index,r.begin,r.len);
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
		return true;
	}
	
	/***
	 * Sends a cancel for given block request.
	 * If remove doesn't match previous add request error will
	 * be thrown. 
	 * Note: requests are removed on receive of block
	 * These are hard sends! [data will be written on call]
	 * The upload and maintenance counter will be updated.
	 * @param r
	 */
	public void cancelRequest(Request r){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send requests on 'connected' connections");
		}else if(peer_choking){
			throw new RuntimeException("Can only send requests on unchoked connections");
		}else if(!ourRequests.contains(r)){
			throw new RuntimeException("Can't canel request that doesn't exist!");
		}
		ourRequests.remove(r);
		try {
			maintenance+=17;
			mp.cancel(sockOut, r.index,r.begin,r.len);
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
		
	}
	
	/**
	 * Takes block response, will throw error if response doesnt
	 * exist in peerRequest set.
	 * These are hard sends! [data will be written on call]
	 * The upload and maintenance counter will be updated.
	 * @param r
	 * @param block
	 */
	public void pushRequestResponse(Request r, byte[] block){
		if(!peerRequests.contains(r)){
			throw new RuntimeException("Giving unrequested block! But whyyyy! =(");
		}else if(conState != ConnectionState.connected){
			throw new RuntimeException("This is invalid connection state isn't in connected mode!");
		}else if(peer_choking){
			throw new RuntimeException("We are being choked! Not valid to send silly!");
		}
		try {
			maintenance+=13;
			upload += block.length;
			mp.piece(sockOut, r.index,r.begin,block);
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
	}
	
	public void pushHave(int index){
		if(conState != ConnectionState.connected){
			throw new RuntimeException("Can only send have on 'connected' connections");
		}else if(!peer_interested){
			throw new RuntimeException("Can only send have on interested connections");
		}
		try {
			maintenance+=10;
			mp.have(sockOut, index);
		} catch (IOException e) {
			conState = ConnectionState.closed;
		}
	}
	
	
	/**
	 * Returns list of active requests.
	 * @author wiselion
	 * @return
	 */
	public List<Request> getPeerRequests(){
		List<Request> q =new ArrayList<Request>(peerRequests.size());
		for(Request r:peerRequests){
			q.add(r);
		}
		return q;
	}
	
	/**
	 * List of blocks gotten since last call.
	 * Note will return null if size 0. 
	 * @return
	 */
	public List<Response> getPeerResponseBlocks(){
		if(peerSentBlocks.size()<1){return null;}
		List<Response> r = peerSentBlocks;
		peerSentBlocks = new ArrayList<Response>();
		return r;
	}
	

	private void doDataIn(PeerMessage pm, Torrent torrent){
		if(pm.type == PeerMessage.Type.CHOKE){
			this.peer_choking = true;
			//Drop our requests. They aint gana get done.
			ourRequests.clear();
		}else if(pm.type == PeerMessage.Type.UNCHOKE){
			this.peer_choking = false;
		}else if(pm.type == PeerMessage.Type.INTERESTED){
			this.peer_interested = true;
		}else if(pm.type == PeerMessage.Type.NOT_INTERESTED){
			this.peer_interested = false;
		}else if(pm.type == PeerMessage.Type.HAVE){
			peerBitMap.addPieceComplete(pm.index);
			if(!am_interested){
				System.out.println("Client sent us have! But WE AIN'T EVEN interested.");
			}
		}else if(pm.type == PeerMessage.Type.BIT_FILED){
			peerBitMap.setBitMap(pm.bitfield);
		}else if(pm.type == PeerMessage.Type.REQUEST){
			peerRequests.add(new Request(pm.index,pm.begin,pm.length));
		}else if(pm.type == PeerMessage.Type.CANCEL){
			peerRequests.remove(new Request(pm.index,pm.begin,pm.length));//should work
		}else if(pm.type == PeerMessage.Type.PIECE){
			//get piece
			Request r = new Request(pm.index,pm.begin,pm.length);
			Response rs = new Response(pm.index,pm.begin,pm.block);
			if(ourRequests.remove(r)){
				download+= pm.block.length;
			}else{
				//For now this isnt a shutdownable event.
				System.out.println("Recieved Piece "+pm.index+","+pm.begin+","+pm.length+" but didnt send request!");
			}
			peerSentBlocks.add(rs);
		}
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof ManagedConnection){
			ManagedConnection m = (ManagedConnection) o;
			return m.ip.equals(ip)&&m.port==port;
		}
		return false;
	}
}
