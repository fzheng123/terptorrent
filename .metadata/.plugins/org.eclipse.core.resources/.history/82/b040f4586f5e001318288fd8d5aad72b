package NetBase;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.concurrent.SynchronousQueue;

import Utils.ByteUtils;

/**
 * Utility class that reads messages in non-blocking mode.
 * Also contains static functions to speak the torrent proto.
 * 
 * 
 * NOT SYNCHRONIZABLE!! So dont do it.
 * 
 */
public class MessageParser {
	/*
	 * 
	 * All of the remaining messages in the protocol take the form of <length prefix><message ID><payload>. 
	 * The length prefix is a four byte big-endian value. 
	 * The message ID is a single decimal byte. The payload is message dependent.
	 * 
	 */
	public static class HandShake{byte[] peerID;byte[] hashInfo;byte[] version;byte[] reserved;}
	public static class PeerMessage{
		public static enum Type {
			CHOKE, UNCHOKE, INTERESTED, NOT_INTERESTED, HAVE, BIT_FILED, REQUEST, PIECE, CANCEL, PORT
		}
		public final Type type;
		///UGHHHH. Bad. but not bad enough.
		byte[] bitfield;
		long piece;
		long index;
		long begin;
		long length;
		byte [] block;
		int port;
		public PeerMessage(Type t){type=t;}
	}
	
	private  SynchronousQueue<PeerMessage> queue= new SynchronousQueue<PeerMessage>();
	private boolean intro;
	private byte [] buffer;
	private int off;
	byte [] sizeBuf;
	private int size;//always the first byte, always in int range.
	public MessageParser(){ intro=false;size=-1;sizeBuf=new byte[4];}
	
	/***
	 * Very simple! Parses current buffer.
	 * To retrieve make a poll request.
	 * @throws IOException 
	 */
	public void readMessage(InputStream in) throws IOException{
		if(!intro){throw new RuntimeException("Haven't called readHandShake() till completion!?!");}
		while(in.available()>0){
			if(size==-1){
				in.read(sizeBuf, off, 4);
				if(off == 4){
					off = 0;
					size = ByteUtils.readInt(sizeBuf,0);
					buffer = new byte[size];
				}
			}else{
				off+=in.read(buffer, off, size);
				if(off==size){
					off=0;//reset state variables.
					size=-1;//reset state variables.
					queue.add(from(buffer));
				}
			}
		}
	}
	
	/***
	 * Called until handShake complete
	 * Non-blocking ^.^
	 * @param in
	 * @return null if not yet complete header
	 * 		   HandShake if complete.
	 * @throws IOException
	 */
	public HandShake readHandShake(InputStream in) throws IOException{
		while (in.available() > 0) {
			if (size == -1) {
				size = in.read();
				buffer = new byte[48 + size];
				off = 0;
			} else {
				off += in.read(buffer, off, 48 + size);
				if (off == 48 + size) {
					HandShake hs = new HandShake();
					hs.version = Arrays.copyOfRange(buffer, 0, size);
					hs.reserved = Arrays.copyOfRange(buffer, size, size + 8);
					hs.hashInfo = Arrays.copyOfRange(buffer, size + 8,size + 8 + 20);
					hs.peerID = Arrays.copyOfRange(buffer, size + 8 + 20,size + 8 + 20 + 20);
					off =0;
					size=-1;
					return hs;
				}
			}
		}
		return null;
	}
	
	
	
	//Ok fine what you had before would have made this a bit quicker....
	private PeerMessage from(byte[] buffer2){
		PeerMessage PM;
		switch(buffer2[0]){
			case 0:
				return PM = new PeerMessage(PeerMessage.Type.CHOKE);
			case 1:
				return PM = new PeerMessage(PeerMessage.Type.UNCHOKE);
			case 2:
				return PM = new PeerMessage(PeerMessage.Type.INTERESTED);
			case 3:
				return PM = new PeerMessage(PeerMessage.Type.NOT_INTERESTED);
			case 4:
				PM = new PeerMessage(PeerMessage.Type.HAVE);
				PM.piece = ByteUtils.readUnsignedInt(buffer2, 1);
				return PM;
			case 5:
				PM = new PeerMessage(PeerMessage.Type.BIT_FILED);
				PM.bitfield = Arrays.copyOfRange(buffer2, 1, buffer2.length);
				return PM;
			case 6:
				PM = new PeerMessage(PeerMessage.Type.REQUEST);
				PM.index = ByteUtils.readUnsignedInt(buffer2, 1);
				PM.begin = ByteUtils.readUnsignedInt(buffer2, 4);
				PM.length = ByteUtils.readUnsignedInt(buffer2, 8);
				return PM;
			case 7:
				PM = new PeerMessage(PeerMessage.Type.PIECE);
				PM.index = ByteUtils.readUnsignedInt(buffer2, 1);
				PM.begin = ByteUtils.readUnsignedInt(buffer2, 4);
				PM.block = Arrays.copyOfRange(buffer2, 8, buffer2.length);
				return PM;
			case 8:
				PM = new PeerMessage(PeerMessage.Type.CANCEL);
				PM.index = ByteUtils.readUnsignedInt(buffer2, 1);
				PM.begin = ByteUtils.readUnsignedInt(buffer2, 4);
				PM.length = ByteUtils.readUnsignedInt(buffer2, 8);
				return PM;
			case 9:
				PM = new PeerMessage(PeerMessage.Type.PORT);
				throw new RuntimeException("Not yet implemented: "+buffer2[0]);
			default:
					throw new RuntimeException("Message parser unknown id: "+buffer2[0]);
		}
	}

	
	public boolean hasMessage(){
		return !queue.isEmpty();
	}
	
	public PeerMessage getNext(){
		return queue.poll();
	}
	
	//I dunno change it if you want. Its just boiler
	//////|||SENDING CODE|||/////
	
	//handshake: <pstrlen><pstr><reserved><info_hash><peer_id>
	public void sendHandShake(OutputStream os,byte [] infoHash,byte[] peerID) throws IOException{
		byte [] sptr = "BitTorrent protocol".getBytes("UTF-8");
		byte [] reserved ={0,0,0,0,0,0,0,0};//eight (8) reserved bytes. All current implementations use all zeroes
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		bo.write(19);
		bo.write(sptr);
		bo.write(reserved);
		bo.write(infoHash);
		bo.write(peerID);
		byte [] o =bo.toByteArray();
		if(o.length!=68){
			throw new RuntimeException("Length incorrect.");
		}
		os.write(o);
	}
	
	
	public void choke(OutputStream os) throws IOException{
		byte [] o = new byte[5];
		o[ByteUtils.writeInt(1,o,0)]=0;//troll
		os.write(o);
	}
	
	public void unchoke(OutputStream os) throws IOException{
		byte [] o = new byte[5];
		o[ByteUtils.writeInt(1,o,0)]=1;//troll
		os.write(o);
	}
	
	public void interested(OutputStream os) throws IOException{
		byte [] o = new byte[5];
		o[ByteUtils.writeInt(1,o,0)]=2;//troll
		os.write(o);
	}
	
	public void not_interested(OutputStream os) throws IOException{
		byte [] o = new byte[5];
		o[ByteUtils.writeInt(1,o,0)]=3;//troll
		os.write(o);
	}
	
	//Rofl we support out but not in.? //TODO: or maybe im just tired?
	public void have(OutputStream os, long index) throws IOException{
		byte [] o = new byte[5+4];
		o[ByteUtils.writeInt(5,o,0)]=4;//troll
		ByteUtils.writeInt(index,o,5);
		os.write(o);
	}
	
	public void bitfield(OutputStream os, byte[] out) throws IOException{
		byte [] o = new byte[5+out.length];
		o[ByteUtils.writeInt(1+out.length,o,0)]=5;//troll
		ByteUtils.writeBytes(out,o,5);
		os.write(o);
	}
	
	public void request(OutputStream os, long index, long begin,long length) throws IOException{
		byte [] o = new byte[13+4];
		o[ByteUtils.writeInt(13,o,0)]=6;//troll
		int i =ByteUtils.writeInt(index,o,5);
		i=ByteUtils.writeInt(begin,o,i);
		ByteUtils.writeInt(length,o,i);
		os.write(o);
	}
	
	public void piece(OutputStream os, long index, long begin,byte [] block) throws IOException{
		byte [] o = new byte[9+4+block.length];
		o[ByteUtils.writeInt(9+block.length,o,0)]=7;//troll
		int i =ByteUtils.writeInt(index,o,5);
		i=ByteUtils.writeInt(begin,o,i);
		ByteUtils.writeBytes(block,o,i);
		os.write(o);
	}
	
	public void cancel(OutputStream os, long index, long begin,long length) throws IOException{
		byte [] o = new byte[13+4];
		o[ByteUtils.writeInt(13,o,0)]=8;//troll
		int i =ByteUtils.writeInt(index,o,5);
		i=ByteUtils.writeInt(begin,o,i);
		ByteUtils.writeInt(length,o,i);
		os.write(o);
	}
	
	//TODO: Port.
	private static byte[] testGen(int len){
		byte [] b =new byte[len];
		for(int i=0;i<len;i++){
			b[i]=(byte) Math.random();
		}
		return b;
	}
	
	/**
	 * How many off by one errors are there going to be?!?
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String [] args) throws IOException{
		//Ok, lets use the potentially broken thing ot test the broken thing
		//right?
		
		MessageParser MP = new MessageParser();
		ByteArrayOutputStream os=new  ByteArrayOutputStream();
		ByteArrayInputStream bis;
		byte [] info = testGen(20);
		byte [] id = testGen(20);
		byte [] bitField = testGen(80);
		MP.sendHandShake(os,info,id);
		bis = new ByteArrayInputStream(os.toByteArray());
		HandShake hs = MP.readHandShake(bis);
		System.out.println("Test 1: "+(Arrays.equals(info,hs.hashInfo)&&Arrays.equals(id,hs.peerID)));
		//MP.bitfield(os, new byte[50]);
		os.reset();
		MP.choke(os);
		bis = new ByteArrayInputStream(os.toByteArray());
		MP.readMessage(bis);
		System.out.println("Test 2: "+(MP.getNext().type==PeerMessage.Type.CHOKE));
		
		
		os.reset();
		MP.unchoke(os);
		bis = new ByteArrayInputStream(os.toByteArray());
		MP.readMessage(bis);
		System.out.println("Test 3: "+(MP.getNext().type==PeerMessage.Type.UNCHOKE));
		
		os.reset();
		MP.interested(os);
		bis = new ByteArrayInputStream(os.toByteArray());
		MP.readMessage(bis);
		System.out.println("Test 4: "+(MP.getNext().type==PeerMessage.Type.INTERESTED));
		
		os.reset();
		MP.not_interested(os);
		bis = new ByteArrayInputStream(os.toByteArray());
		MP.readMessage(bis);
		System.out.println("Test 5: "+(MP.getNext().type==PeerMessage.Type.NOT_INTERESTED));
		
		os.reset();
		MP.bitfield(os,bitField);
		bis = new ByteArrayInputStream(os.toByteArray());
		MP.readMessage(bis);
		System.out.println("Test 6: "+(Arrays.equals(bitField, MP.getNext().bitfield)));
		
		os.reset();
		MP.have(os,10241139);
		bis = new ByteArrayInputStream(os.toByteArray());
		MP.readMessage(bis);
		System.out.println("Test 7: "+(MP.getNext().piece==10241139));
		
		os.reset();
		MP.request(os, 10199,10211,12311);
		bis = new ByteArrayInputStream(os.toByteArray());
		MP.readMessage(bis);
		PeerMessage p = MP.getNext();
		System.out.println("Test 7: "+(p.begin==10211&&p.index==10199&&p.length==12311));
		
		byte [] block = testGen(10234);
		os.reset();
		MP.piece(os, index, begin, block);
		bis = new ByteArrayInputStream(os.toByteArray());
		MP.readMessage(bis);
		PeerMessage p = MP.getNext();
		System.out.println("Test 7: "+(p.begin==10211&&p.index==10199&&p.length==12311));
		
		
		
	}
	
}
