package NetBase;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Random;

import NetBase.HttpResponse.HeaderType;
import TorrentData.Torrent;
import Utils.Bencoding;
import Utils.Bencoding.Type;
import Utils.ByteUtils;

public class Tracker {
	public static enum Event{started,stopped,completed,regular};
	
	URL url;
	String strUrl;
	int port;
	InetAddress address;
	String id;
	public Tracker(String urlString){
		try {
			if(urlString.substring(0, 3).equals("udp")){
				String[] token = urlString.split("/");
				System.out.println(token[2]);
				if(token[2].contains(":")){
					token = token[2].split(":");
					address = InetAddress.getByName(token[0]);
					port = Integer.parseInt(token[1]);
				}else{
					address = InetAddress.getByName(token[2]);
					port = 80;
				}
			}else{
				url = new URL(urlString);
				port = url.getPort();
				if(port==-1){
					port = 80;
				}
			}
			//address = InetAddress.getByName(url.getHost());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/***
	 * Updates torrent file with results from tracker
	 * If tracker gives bad results it prints out the http response.
	 * 
	 * @note Recently added robustness. sent time out after 8 seconds of no recv
	 * 
	 *  
	 * @param torrent
	 * @param event
	 */
	public void getTrackerResults(Torrent torrent,Event event){
		//form Get request
		// ",page,string,peerID,bytes,31415,ip
		try {
			String gq = url.getQuery();
			if(gq==null){
				gq="?";
			}else{
				gq="?"+gq+"&";
			}
			
			String e;
			if(event == Event.started){
				e="&event=started";
			}else if(event == Event.stopped){
				e="&event=stopped";
			}else if(event == Event.completed){
				e = "&event=completed";
			}else if(id!=null){
				e = ""+id;
			}else{
				e="";
			}
			
			String getRequest = "GET "+url.getPath()+gq+"info_hash="
					+ torrent.urlEncodedHash + "&peer_id=" + torrent.peerID
					+ "&uploaded=" + torrent.getUploaded() + "&downloaded="
					+ torrent.getDownloaded() + "&left=" + torrent.getLeft()
					+ "&compact=1" + "&port=" +torrent.uPnP_Port + e+" HTTP/1.1\r\nHost: "+url.getHost()+"\r\n\r\n";
			
			System.out.println(getRequest);
			//TODO: report status to console
			Socket socket = new Socket();
			socket.setSoTimeout(8*1000);
			socket.connect(new InetSocketAddress(url.getHost(), port));
			OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
			osw.write(getRequest);
			osw.flush();
			
			HttpResponse response = new HttpResponse(socket.getInputStream());
			socket.close();
			//TODO: output log
			System.out.println("Response Status: "+response.status);
			System.out.println("Response length: "+response.contentSize);
			System.out.println("Actual length: "+response.body.length);
			System.out.println("ResponseOut:\n "+new String(response.body,"UTF-8"));
			System.out.println("Uknown:\n "+response.headerMap.get(HeaderType.UKNOWN));
			System.out.println("\n\n");
			if(response.status == 200){
				Bencoding b;
				try{
					b = new Bencoding(response.body);
				}catch (Exception ex){
					System.out.println("Tracker Connected. But response Not BENCODED?!");
					ex.printStackTrace();
					return;
				}
				
				if(b.dictionary.containsKey("failure reason")){
					System.out.println("Tracker Connected. But bencoded response failed with \""+b.dictionary.get("failure reason").getString());
				}else{
					System.out.println("Tracker Results: ");
					torrent.announceInterval = b.dictionary.get("interval").integer;
					if( b.dictionary.get("peers").type==Type.String){
						byte[] peers = b.dictionary.get("peers").byteString;
						for(int i=0;i<peers.length/6;i++){
							byte []addr = new byte[4];
							System.arraycopy(peers, i*6, addr, 0, 4);
							InetAddress ip = InetAddress.getByAddress(addr);
							int port =((peers[i*6+4]&0xFF) << 8 | (peers[i*6+5]&0xFF));
							System.out.println("Peer: "+ip.getHostAddress()+":"+port);
							torrent.addPeer(ip,port,null);
						}
						
					}else if(b.dictionary.get("peers").type == Type.List){
						List<Bencoding> list=b.dictionary.get("peers").list;
						for(Bencoding dict : list){
							InetAddress ip = InetAddress.getByName(new String(dict.dictionary.get("ip").byteString,"UTF-8"));
							int port = new Integer(new String(dict.dictionary.get("ip").byteString,"UTF-8"));
							torrent.addPeer(ip,port,null);
						}
					}else{
						System.out.println("Peers is not dictionary or string.");
					}
					
					if(b.dictionary.containsKey("tracker id")){
						id = new String(b.dictionary.get("tracker id").byteString,"UTF-8");
						System.out.println("Trackerid:\""+id+"\"");
					}
					
				}
				
				
				
				
			}else{
				System.out.println("Response: "+response.status);
				System.out.println("ResponseOut:\n "+new String(response.body,"UTF-8"));
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void getUPDTrackerResult(Torrent t,Event e){
		DatagramSocket sock = null;
		short clientPort = 15000;
		try {
			clientPort = 15000;
			sock = new DatagramSocket(clientPort);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//Start connection message
		DatagramPacket sendPacket  = null, recvPacket = null;
		byte[] sendBuffer = null, recvBuffer = null;
		long conID = 0x41727101980l;
		Random r = new Random();
		int transactionID = r.nextInt();
		recvBuffer = new byte[65508];
		recvPacket = new DatagramPacket(recvBuffer,recvBuffer.length);
		ByteBuffer message = ByteBuffer.allocate(98);
		sock.connect(address,port);
		System.out.println("Test");
		message.putLong(conID);
		message.putInt(UDPAction.CONNECT.value);	
		message.putInt(transactionID);
		sendBuffer = message.array();
		sendPacket = new DatagramPacket(sendBuffer,sendBuffer.length,address,port);
		byte[] reponse = null;
		try {
			sock.send(sendPacket);
			sock.receive(recvPacket);
			reponse = recvPacket.getData();
			if(reponse!=null){
				 // Start Announce 
				 int actionIdFromResponse = ByteUtils.readInt(reponse, 0);
				 int transactionIdFromResponse = ByteUtils.readInt(reponse, 4);
				 long connectionIdFromResponse = ByteUtils.readUnsignedInt(reponse, 8);
				 transactionID = r.nextInt();
				 int event = 0;
					if(e == Event.started){
						event =2;
					}else if(e == Event.stopped){
						event =3;
					}else if(e == Event.completed){
						event =1;
					}
				int peersWanted = 20;
				 sendBuffer = getAnnounceInput(conID,transactionID,t.getInfoHash(),t.getPeerID(),t.getDownloaded(),t.totalBytes,0,event,peersWanted,clientPort);
				sendPacket = new DatagramPacket(sendBuffer,sendBuffer.length,address,port);
				sock.send(sendPacket);
				sock.receive(recvPacket);
				reponse = recvPacket.getData();
				if(reponse!=null){
					if(reponse.length >= 20){
					int recTransId = ByteUtils.readInt(reponse, 4);
					int interval = ByteUtils.readInt(reponse, 8);
					int leechers = ByteUtils.readInt(reponse, 12);
					int seeders = ByteUtils.readInt(reponse, 16);
					short peerPort=-1;
					for(int i= 0;i<(reponse.length-20)/6&&peerPort!=0;i++){
						byte[] rawIp = new byte[4];
						System.arraycopy(reponse, 20+i*6, rawIp, 0, 4);
						InetAddress ip = InetAddress.getByAddress(rawIp); 
						peerPort = ByteUtils.readShort(reponse, 24+6*i);
						if(peerPort!=0){
							t.addPeer(ip,peerPort, null);
							System.out.println("Peer: "+ip.getHostAddress()+":"+port);
						}
					}
					}else{
						System.err.println("ERROR");
					}
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	public byte[] getAnnounceInput(long conId, int transId,byte[] infoHash, byte[] peerId,long downloaded,long left, long uploaded, int event,int numWanted, short port){
		ByteBuffer message = ByteBuffer.allocate(98);
		message.putLong(conId);
		message.putInt(UDPAction.ANNOUNCE.value);
		message.putInt(transId);
		message.put(infoHash);
		message.put(peerId);
		message.putLong(downloaded);
		message.putLong(left);
		message.putLong(uploaded);
		message.putInt(event);
		message.putInt(0);
		message.putInt(0);
		message.putInt(20);
		message.putShort(port);
		return message.array();
	}
 }
