package NetBase;

public enum UDPAction {
	CONNECT(0),
	ANNOUNCE(1),
	SCRAPE(2),
	ERROR(3);
	int value;
	UDPAction(int value){
		this.value = value;
	}
}
