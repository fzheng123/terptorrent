package Primitives;

import java.io.File;

/**
 * Pieces may overlap file boundaries.
 * Well. Crap. Use byte offset to calculate piece info.
 */
public class DownloadFile {
	
	protected long length, byteOffSet;//TODO: offset zero based?
	protected File f;
	public DownloadFile(String topFolder,String pathAndName,long length,long byteOff){
		this.length=length;
		this.byteOffSet=byteOff;
		f = new File(topFolder+"\\"+pathAndName);
	}
	
	
	public long getOffSet(){
		return byteOffSet;
	}
	
	public long getLength(){
		return length;
	}
	
	public File getFileHandle(){
		return f;
	}
	
}
