package Logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import NetBase.ManagedConnection;
import NetBase.ManagedConnection.ConnectionState;
import NetBase.MessageParser.Request;
import NetBase.MessageParser.Response;
import Primitives.BitMap.Rarity;
import Primitives.Piece;
import TorrentData.Torrent;

/**
 * This is the implementation of peer logic
 * This is probably the most basic form of a client.
 * 
 * It follows the following rules:
 * -if some one requests something that we can give:
 * 	*give it to them
 * -collect list of rarity pieces we dont have and disseminate get requests for them 
 * 
 * 
 * 
 * @author wiselion
 *
 */
public class BasicPeerLogic extends PeerLogic{
	private Set<Integer> deceminatedPieces = new HashSet<Integer>();
	private Map<ManagedConnection, Set<Piece>> clientToPieceSet = new HashMap<ManagedConnection, Set<Piece>>();
	private List<Piece> recentlyCompletedPieces = new ArrayList<Piece>();
	private List<Piece> completedLast = new ArrayList<Piece>();
	private static int MAX_ENQEUED = 5;
	
	private boolean readBlocks(Torrent t,List<Response> responses){
		
		return true;
	}
	
	@Override
	public void doWork(Torrent t) throws IOException {
		Set<ManagedConnection> pList = t.getPeers();
		Iterator<ManagedConnection> itor = pList.iterator();
		
		
		while(itor.hasNext()){
			ManagedConnection p = itor.next();
			if(p.getConnectionState() == ConnectionState.uninitialized){
				p.initalizeConnection(t.getBitMap().getMapCopy());
				clientToPieceSet.put(p,new HashSet<Piece>());
			}else if(p.getConnectionState() == ConnectionState.closed){
				//pull off anything new?
				itor.remove();
				t.getBitMap().removePeerMap(p.getPeerBitmap());
			}else if(p.getConnectionState() == ConnectionState.connected){
				//Read state. 
				//Push requests (if allowed)
				//Push blocks (if allowed)
				Set<Piece> ps = clientToPieceSet.get(p);
				p.doWork(t);
				t.getBitMap().addPeerMap(p.getPeerBitmap());
				if(p.amChoking()){
					p.setAmChoking(false);
				}
				if(!p.amInterested()){
					p.setAmInterested(true);
				}
				
				//if not choking:
				readBlocks(t,p.getPeerResponseBlocks());
				if(!p.peerChoking()){
					//send out thier requests
					for(Request r : p.getPeerRequests()){
						if(t.pm.hasPiece(r.index)){
							Piece piece = t.pm.getPiece(r.index);
							if(piece!=null){
								p.pushRequestResponse(r, piece.getFromComplete(r.begin, r.len));
							}
						}
					}
					
					//now send for our requests
					Iterator<Piece>  pI = ps.iterator();
					//Note that this is only doing one part of one piece
					//TODO: not this stupid.
					while(pI.hasNext()){
						Piece piece = pI.next();
						if(piece.isComplete()){
							recentlyCompletedPieces.add(piece);
							pI.remove();
						}else{
							Request r =piece.getNextBlock();
							p.addRequest(r);
						}
					}
					
				}
				
				if(p.peerInterested()){
					for(Piece piece : completedLast){
						p.pushHave((int) piece.pieceIndex);
					}
				}
				
				
				
			}
		}
		
		completedLast.clear();
		completedLast.addAll(recentlyCompletedPieces);
		//TODO: makes more sense to keep bitmap in pieceManager.
		for(Piece p : recentlyCompletedPieces){
			t.pm.putPiece(p);
			t.getBitMap().addPieceComplete(p.pieceIndex);
			deceminatedPieces.remove((int)p.pieceIndex);
		}
		
		//Do once every x:                                                                                                                                                                                                                                                                                                                                                       
		t.getBitMap().recomputeRarity();
		int mcInex = 0;
		ManagedConnection[] connections = pList.toArray(new ManagedConnection[0]);
		//Distribute evenly amoungst peers:
		//TODO: elegent loop. This is fugly
		for(Rarity r: t.getBitMap().getRarity()){
			if(!t.getBitMap().hasPiece(r.index) && !deceminatedPieces.contains((int)r.index)){
				boolean loopOnce = false;
				boolean loopTwice = false;
				for(int i = mcInex;i<connections.length;i++){
					if(mcInex>=connections.length){
						if(loopOnce){loopTwice=true;break;}
						loopOnce=true;mcInex=0;i=0;
					}
					ManagedConnection mc = connections[i];
					Set<Piece> queuedData = clientToPieceSet.get(connections[i]);
					if(mc.getConnectionState() == ConnectionState.connected && !mc.peerChoking()&&
							queuedData.size()<MAX_ENQEUED){
						Piece p = t.getBitMap().createMissingPiece(r.index);
						queuedData.add(p);
						break;
					}
					
				}
				if(loopTwice){
					break;
				}
			}
		}
		
		t.pm.doBlockingWork();//Ughhh what do i do for this?
	}
	

}
